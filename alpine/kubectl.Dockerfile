FROM alpine:3

RUN apk add curl bash git openssl
RUN curl -LO https://dl.k8s.io/release/v1.29.2/bin/linux/amd64/kubectl
RUN chmod +x ./kubectl
RUN mv ./kubectl /usr/local/bin/kubectl
