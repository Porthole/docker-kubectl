FROM porthol/helm:3-on-alpine-3

RUN apk add nodejs npm
RUN curl -L https://github.com/mikefarah/yq/releases/latest/download/yq_linux_amd64 -o /usr/local/bin/yq \
    && chmod a+x /usr/local/bin/yq

RUN apk add curl bash git openssl
RUN curl -LO https://dl.k8s.io/release/v1.29.2/bin/linux/amd64/kubectl
RUN chmod +x ./kubectl
RUN mv ./kubectl /usr/local/bin/kubectl
